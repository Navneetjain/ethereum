package com.darwinlabs.test.ledgerex.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.FloatProperty;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.darwinlabs.test.ledgerex.R;

public class SendActivity extends AppCompatActivity {
    public static final String TAG = "PinLockView";

    Toolbar toolbar;
    private PinLockView mPinLockView;
    ImageView forword_button;
    String patternPin="0";
    TextView ethValue_tv,usdValue_tv;
    double usdConvertingValue;
    private PinLockListener pinLockListener=new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            Log.d(TAG, "Pin complete: " + pin);
            patternPin=pin;

        }

        @Override
        public void onEmpty() {
            Log.d(TAG, "Pin empty");
            ethValue_tv.setText("0"+"  ETH");
            usdValue_tv.setText("= " +"0"+"  USD");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
            patternPin=intermediatePin;
            usdConvertingValue= (Double.parseDouble(intermediatePin))*1.5;
            Log.d("ValueOfPin","pin---"+patternPin+"--conveted---"+usdConvertingValue);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ethValue_tv.setText(patternPin+"  ETH");
                    usdValue_tv.setText("= " +usdConvertingValue+"  USD");
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_send);
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        ethValue_tv= (TextView) findViewById(R.id.ethValue_tv);
        usdValue_tv= (TextView) findViewById(R.id.usdValue_tv);

        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        forword_button= (ImageView) findViewById(R.id.forword_button);
        toolbar.setTitle("SEND");
        toolbar.setNavigationIcon(R.drawable.backicon);
        setSupportActionBar(toolbar);
        mPinLockView.setPinLockListener(pinLockListener);
        mPinLockView.setPinLength(12);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.backspace, getTheme());
        mPinLockView.setDeleteButtonDrawable(drawable);
        forword_button.setOnClickListener(sendClick);

    }
    View.OnClickListener sendClick=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
//            Intent intent=new Intent(SendActivity.this,HomeActivity.class);
//            startActivity(intent);
            finish();
        }
    };
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
