package com.darwinlabs.test.ledgerex.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.darwinlabs.test.ledgerex.R;

public class LoginActivity extends AppCompatActivity {

    public static final String TAG = "PinLockView";

    Toolbar toolbar;
    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;
    ImageView forword_button;
    String patternPin;
    private PinLockListener pinLockListener=new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            Log.d(TAG, "Pin complete: " + pin);
            patternPin=pin;
        }

        @Override
        public void onEmpty() {
            Log.d(TAG, "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
            patternPin=intermediatePin;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        forword_button= (ImageView) findViewById(R.id.forword_button);
        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);
        toolbar.setTitle("ENTER PIN");
        toolbar.setNavigationIcon(R.drawable.backicon);
        setSupportActionBar(toolbar);

        mPinLockView.attachIndicatorDots(mIndicatorDots);
        mPinLockView.setPinLockListener(pinLockListener);

        mPinLockView.setPinLength(10);
        mPinLockView.setTextColor(ContextCompat.getColor(this, R.color.grey));
        mPinLockView.setDeleteButtonPressedColor(R.color.grey);

        mIndicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FILL_WITH_ANIMATION);
        Drawable deleteDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.backspace, getTheme());
        forword_button.setOnClickListener(forwordClick);
        mPinLockView.setDeleteButtonDrawable(deleteDrawable);
    }
    View.OnClickListener forwordClick=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (patternPin!=null&&patternPin.equals("1234")){
                Intent intent=new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }else {
                Toast.makeText(LoginActivity.this,"Wrong PIN",Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
