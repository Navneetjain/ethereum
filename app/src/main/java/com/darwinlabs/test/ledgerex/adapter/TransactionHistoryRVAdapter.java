package com.darwinlabs.test.ledgerex.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.darwinlabs.test.ledgerex.R;
import com.darwinlabs.test.ledgerex.model.TransactionModel;

import java.util.List;

/**
 * Created by i5tagbin2 on 3/9/17.
 */

public class TransactionHistoryRVAdapter extends RecyclerView.Adapter<TransactionHistoryRVAdapter.MyViewHolder> {
    Context context;
    List<TransactionModel> transactionModelList;

    public TransactionHistoryRVAdapter(Context context1,List<TransactionModel> transactionModelList1){
        context=context1;
        transactionModelList=transactionModelList1;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.transaction_history_item, parent, false);
        MyViewHolder mvh=new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.transactionDate.setText(transactionModelList.get(position).getDate());
        holder.transactionTime.setText(transactionModelList.get(position).getTime());
        holder.transactionAmount.setText(transactionModelList.get(position).getTransaction());
    }

    @Override
    public int getItemCount() {
        return transactionModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView transactionDate,transactionTime,transactionAmount;
        public MyViewHolder(View itemView) {
            super(itemView);
            transactionDate=(TextView)itemView.findViewById(R.id.transactionDate);
            transactionTime=(TextView)itemView.findViewById(R.id.transactionTime);
            transactionAmount=(TextView)itemView.findViewById(R.id.trsactionAmount_tv);
        }
    }
}
