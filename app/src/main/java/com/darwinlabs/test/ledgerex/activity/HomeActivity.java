package com.darwinlabs.test.ledgerex.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.darwinlabs.test.ledgerex.R;
import com.darwinlabs.test.ledgerex.adapter.NavigationDrawerAdapter;
import com.darwinlabs.test.ledgerex.adapter.TransactionHistoryRVAdapter;
import com.darwinlabs.test.ledgerex.model.TransactionModel;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToggle;
    RecyclerView mDrawerList;
    NavigationDrawerAdapter navadapter;
    Toolbar toolbar;
    ImageView sendImage,receiveImage,buySellImage;
    RecyclerView transactionHistory_Rv;
    TransactionHistoryRVAdapter transactionHistoryRVAdapter;
    List<TransactionModel> transactionList;
    TransactionModel transactionModel;
    List<String> drawerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        sendImage= (ImageView) findViewById(R.id.sendMoneyImage);
        receiveImage= (ImageView) findViewById(R.id.recieveMoneyImage);
        buySellImage= (ImageView) findViewById(R.id.buyImage);
        mDrawerList = (RecyclerView) findViewById(R.id.left_drawer);
        transactionHistory_Rv= (RecyclerView) findViewById(R.id.transactionHistory_rv);
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("LedgerEX");
        drawerList=new ArrayList<>();

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,R.string.drawer_open, R.string.drawer_close) {

            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        navadapter.notifyDataSetChanged();
                    }
                });
                invalidateOptionsMenu();
            }
            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getSupportActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
        navadapter = new NavigationDrawerAdapter(this);
        mDrawerList.setLayoutManager(new LinearLayoutManager(this));
        mDrawerList.setAdapter(navadapter);

        mDrawerToggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.menuicon, getTheme());
        mDrawerToggle.setHomeAsUpIndicator(drawable);


        sendImage.setOnClickListener(sendClick);
        receiveImage.setOnClickListener(receiveCick);
        buySellImage.setOnClickListener(buySellClick);
        transactionList=new ArrayList<>();
        for (int i=0;i<5;i++){
            transactionModel=new TransactionModel();
            transactionModel.setDate("3 september 2017");
            transactionModel.setTime("3:55 PM");
            transactionModel.setTransaction("0.234556 ETH");
            transactionList.add(transactionModel);

        }
        transactionHistoryRVAdapter=new TransactionHistoryRVAdapter(HomeActivity.this,transactionList);
        transactionHistory_Rv.setLayoutManager(new LinearLayoutManager(HomeActivity.this,LinearLayoutManager.VERTICAL,false));
        transactionHistory_Rv.setHasFixedSize(true);
        transactionHistory_Rv.setNestedScrollingEnabled(false);
        transactionHistory_Rv.setAdapter(transactionHistoryRVAdapter);
        transactionHistory_Rv.setFocusable(false);


    }
    View.OnClickListener sendClick=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent=new Intent(HomeActivity.this,SendActivity.class);
            startActivity(intent);
        }
    };
    View.OnClickListener receiveCick=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent=new Intent(HomeActivity.this,ReceiveActivity.class);
            startActivity(intent);
        }
    };
    View.OnClickListener buySellClick=new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
            mDrawerLayout.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDrawerLayout.closeDrawers();
    }
}
