package com.darwinlabs.test.ledgerex.model;

/**
 * Created by i5tagbin2 on 3/9/17.
 */

public class TransactionModel {
    String date,time,transaction;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }
}
