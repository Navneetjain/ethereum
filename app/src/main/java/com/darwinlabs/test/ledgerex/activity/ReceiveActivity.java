package com.darwinlabs.test.ledgerex.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.darwinlabs.test.ledgerex.R;
import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ReceiveActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    Toolbar toolbar;
    private ZXingScannerView mScannerView;
    final int REQUEST_LOCATION = 2;
    private static String[] PERMISSIONS_LOCATION = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA};
    ViewGroup contentFrame;
    TextView ethereumResult_tv;
    ImageView copyImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive);
        contentFrame = (ViewGroup) findViewById(R.id.content_frame);
        ethereumResult_tv= (TextView) findViewById(R.id.etherumresult_tv);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        contentFrame.addView(mScannerView);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        copyImage= (ImageView) findViewById(R.id.copyImage);
        toolbar.setTitle("RECEIVE");
        toolbar.setNavigationIcon(R.drawable.backicon);
        setSupportActionBar(toolbar);
        if (hasLocationPermissionGranted()){
            mScannerView.startCamera();
        }
        else{
            requestLocationPermission();
        }
        copyImage.setOnClickListener(copyClick);
    }
    View.OnClickListener copyClick=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
//            Intent intent=new Intent(ReceiveActivity.this,HomeActivity.class);
//            startActivity(intent);
            finish();
        }
    };
    @Override
    public void handleResult(Result rawResult) {
        Log.v("scanner", rawResult.getText()); // Prints scan results
        Log.v("scanner", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)
        String serial_no=rawResult.getText();
        ethereumResult_tv.setText(serial_no);



        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScannerView.resumeCameraPreview(ReceiveActivity.this);
            }
        }, 2000);
    }
    public boolean hasLocationPermissionGranted() {
        return ContextCompat.checkSelfPermission(ReceiveActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ReceiveActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestLocationPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            ActivityCompat.requestPermissions(ReceiveActivity.this, PERMISSIONS_LOCATION,
                    REQUEST_LOCATION);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //Storage permission is enabled
                        mScannerView.startCamera();

                    } else if (ActivityCompat.shouldShowRequestPermissionRationale(ReceiveActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(ReceiveActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)&& ActivityCompat.shouldShowRequestPermissionRationale(ReceiveActivity.this, Manifest.permission.CAMERA)) {
                        //User has deny from permission dialog
                        final AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(ReceiveActivity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setTitle("Reading Permission Denied");
                        alertDialog1.setMessage("Are you sure you want to deny this permission?");
                        alertDialog1.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.setNegativeButton("RETRY", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.show();
                    } else {
                        // User has deny permission and checked never show permission dialog so you can redirect to Application settings page
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(ReceiveActivity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setMessage("It looks like you have turned off permission required for this feature. It can be enabled under Phone Settings > Apps > LedgerEx > Permissions");
                        alertDialog1.setPositiveButton("GO TO SETTINGS", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivity(intent);
                            }
                        });
                        alertDialog1.show();
                    }
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
